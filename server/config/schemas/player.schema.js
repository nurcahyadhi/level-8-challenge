module.exports = {
  type: "object",
  properties: {
    username: {
      type: "string",
      description: "this is username of user",
      required: true,
      example: "nurcahyadhi",
    },
    email: {
      type: "string",
      description: "user's email",
      required: true,
      example: "nurcahyadhi@mail.com",
    },
    password: {
      type: "string",
      description: "user's password",
      required: true,
    },
  },
};
