module.exports = {
  type: "object",
  properties: {
    exp: {
      type: "integer",
      description: "experience value",
      required: true,
      example: 200,
    },
  },
};
